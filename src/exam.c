       #include <unistd.h>
       #include <stdlib.h>
       #include <stdio.h>
            // déclaration de variable 
          int opt;
          FILE *inputFile; // Déclaration d'un pointeur de fichier pour stocker le fichier à lire
          FILE *outputFile;
          int c;
         char i; // Déclaration de la variable pour stocker chaque caractère lu
         /* fonctions */
           void exercice1(int argc, char *argv[]){
                  inputFile = fopen(argv[2], "r"); // Ouvre le fichier donné en argument en mode lecture

    if (inputFile == NULL) // Si l'ouverture du fichier échoue
    {
        perror("Un problème est survenu : "); 

        abort();
    }

    printf("Ce fichier contient : \n"); 

    do
    {
        i = fgetc(inputFile); // Lit un caractère du fichier
        printf("%c", i);
    } while (i != EOF); // Boucle jusqu'à ce qu'on atteigne la fin du fichier

    printf("\n");

    fclose(inputFile); // Ferme le fichier

    return;  
            
    }

           void exercice2(int argc, char *argv[]){
            if (argc == 1) {
      
        while ((c = getchar()) != EOF) {
            putchar(c);
        }
    } else {
        for (int i = 2; i < argc; i++) {
            inputFile = fopen(argv[i], "r");
            outputFile = fopen("../samples/output.txt", "w");
            if (inputFile == NULL) {
                printf(" %s: le fichier ne correspond a aucun fichier\n", argv[i]);
                return;
            }
            
            while ((c = fgetc(inputFile)) != EOF) {
                // putchar(c);
                
               fputc(c,outputFile);
            }

            fclose(inputFile);
            fclose(outputFile);
        }
    
    }


           }  
      int main(int argc, char *argv[])
       {


           while ((opt = getopt(argc, argv, "pi:")) != -1) {
               switch (opt) {
               case 'p':
                exercice1(argc,argv);
                   break;
               case 'i':
               exercice2(argc,argv);
                   break;
                   exit(EXIT_FAILURE);
               }
           }



           if (optind >= argc) {
               fprintf(stderr, "Choississez un opérateur (a b c d )\n");
               exit(EXIT_FAILURE);
           }

           printf("name argument = %s\n", argv[optind]);

         

    return 0;  
       }