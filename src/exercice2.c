
#include <stdlib.h>
#include <stdio.h>
int main(int argc, char *argv[]) {
    FILE *inputFile;
    FILE *outputFile;
    int c;
    if (argc == 1) {
      
        while ((c = getchar()) != EOF) {
            putchar(c);
        }
    } else {
        for (int i = 1; i < argc; i++) {
            inputFile = fopen(argv[i], "r");
            outputFile = fopen("../samples/output.txt", "w");
            if (inputFile == NULL) {
                printf(" %s: le fichier ne correspond a aucun fichier\n", argv[i]);
                return 1;
            }
            
            while ((c = fgetc(inputFile)) != EOF) {
                // putchar(c);
                
               fputc(c,outputFile);
            }

            fclose(inputFile);
            fclose(outputFile);
        }
    
    }

    return 0;  
}